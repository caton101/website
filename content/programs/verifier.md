+++
title="Verifier"
+++

{{ specialimage(
    class="programIcon",
    link="/assets/program-art/verifier/icon.png"
)}}

I am always skeptical of file transfers and if the file is still safe. This
program scans and determines if there are any issues with transferred files.

<div class="programSplitView">
<div class="programSplitChild">

## Downloads

{{ downloadpanel(
    linux="https://ftp.code-scribe.com/verifier/Verifier Linux.zip",
    macos="https://ftp.code-scribe.com/verifier/Verifier macOS.zip",
    source="https://gitlab.com/caton101/verifier"
)}}

</div>
<div class="programSplitChild">

## How To Install

1. Download the zip file for your operating system.
2. Extract the zip file.
3. Run the install script.

</div>
</div>
