+++
title="Program Name"
draft=true
+++

{{ specialimage(
    class="programIcon",
    link="/assets/program-art/unknown/icon.png"
)}}

This is the description of your program.

<div class="programSplitView">
<div class="programSplitChild">

## Downloads

**Note**: To disable a download, just don't set that variable.

{{ downloadpanel(
    linux="https://ftp.code-scribe.com/program-name/Program Name Linux.zip",
    windows="https://ftp.code-scribe.com/program-name/Program Name Windows.zip",
    macos="https://ftp.code-scribe.com/program-name/Program Name macOS.zip",
    source="https://gitlab.com/caton101/repo"
)}}

</div>
<div class="programSplitChild">

## How To Install

1. Step 1.
2. Step 2.
3. Step 3.

</div>
</div>
