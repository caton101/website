+++
title="To-Do List 2"
+++

{{ specialimage(
    class="programIcon",
    link="/assets/program-art/to-do-list-2/icon.png"
)}}

After using my old to-do list program for several years, I decided to write a
better version. It supports time zones, multiple lists, and an API.

<div class="programSplitView">
<div class="programSplitChild">

## Downloads

{{ downloadpanel(
    linux="https://gitlab.com/caton101/todo-list-2/-/releases",
    macos="https://gitlab.com/caton101/todo-list-2/-/releases",
    source="https://gitlab.com/caton101/todo-list-2"
)}}

</div>
<div class="programSplitChild">

## How To Install

1. Download the zip file for your operating system.
2. Extract the zip file.
3. Run `make install`.

</div>
</div>
