+++
title="Web Daddy"
+++

{{ specialimage(
    class="programIcon",
    link="/assets/program-art/web-daddy/icon.png"
)}}

There is a shocking lack of drag-and-drop website updating software for Linux.
This program checks a git repository for updates and then upgrades the web
server. It is designed to run locally on the web server and does not need to
be maintained once installed.

<div class="programSplitView">
<div class="programSplitChild">

## Downloads

{{ downloadpanel(
    linux="https://ftp.code-scribe.com/web-daddy/Web Daddy Linux.zip",
    source="https://gitlab.com/caton101/web-daddy"
)}}

</div>
<div class="programSplitChild">

## How To Install

1. Download the zip file for your operating system.
2. Extract the zip file.
3. Run the install script.

</div>
</div>
