+++
title="Programs"
+++

From shell scripts to utilities to user applications, there is something for
everyone. What are you waiting for? Give one of them a try.

---

{{ programlistingr(
    name="HyCat",
    description="A cat clone with pride gradients.",
    image="/assets/program-art/hycat/icon.png",
    link="/programs/hycat"
)}}

---

{{ programlisting(
    name="Speak N Spell Emulator",
    description="This program emulates a Speak & Spell toy.",
    image="/assets/program-art/speak-n-spell/icon.png",
    link="/programs/speak-n-spell"
)}}

---

{{ programlistingr(
    name="To-Do List",
    description="A simple To-Do list manager. (Old Version)",
    image="/assets/program-art/to-do-list/icon.png",
    link="/programs/to-do-list"
)}}

---

{{ programlisting(
    name="To-Do List 2",
    description="A command line To-Do list manager.",
    image="/assets/program-art/to-do-list-2/icon.png",
    link="/programs/to-do-list-2"
)}}

---

{{ programlistingr(
    name="Verifier",
    description="A utility for verifying file integrity.",
    image="/assets/program-art/verifier/icon.png",
    link="/programs/verifier"
)}}

---

{{ programlisting(
    name="Web Daddy",
    description="A tool for publishing your website's Git repository to your web server.",
    image="/assets/program-art/web-daddy/icon.png",
    link="/programs/web-daddy"
)}}
