+++
title="To-Do List"
+++

{{ specialimage(
    class="programIcon",
    link="/assets/program-art/to-do-list/icon.png"
)}}

After needing a way to organize my schedule, I wrote a small terminal
application. It has basic features like editing events and marking them
complete.

**NOTE**: This is the old version. Please use the new one.

{{ urlbutton(
    link="/programs/to-do-list-2",
    message="To-Do List 2"
)}}

<div class="programSplitView">
<div class="programSplitChild">

## Downloads

{{ downloadpanel(
    linux="https://ftp.code-scribe.com/to-do-list/To-Do List Linux.zip",
    macos="https://ftp.code-scribe.com/to-do-list/To-Do List macOS.zip",
    source="https://gitlab.com/caton101/todo-list"
)}}

</div>
<div class="programSplitChild">

## How To Install

1. Download the zip file for your operating system.
2. Extract the zip file.
3. Run the install script.

</div>
</div>
