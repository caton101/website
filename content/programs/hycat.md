+++
title="HyCat"
draft=false
+++

{{ specialimage(
    class="programIcon",
    link="/assets/program-art/hycat/icon.png"
)}}

Inspired by `lolcat` and `hyfetch`, this cat clone concatenates text using pride
gradients. The color gradient can be changed with the built-in configuration
tool.

<div class="programSplitView">
<div class="programSplitChild">

## Downloads

{{ downloadpanel(
    linux="https://gitlab.com/caton101/hycat/-/releases",
    source="https://gitlab.com/caton101/hycat"
)}}

</div>
<div class="programSplitChild">

## How To Install

1. Download the latest release
2. Unzip the archive
3. Enter the extracted directory
4. Run `make install` in a terminal

</div>
</div>
