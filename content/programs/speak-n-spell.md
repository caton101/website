+++
title="Speak N Spell Emulator"
+++

{{ specialimage(
    class="programIcon",
    link="/assets/program-art/speak-n-spell/icon.png"
)}}

Inspired by the 1970's, this program emulates the core functionality of a Speak
N Spell.

<div class="programSplitView">
<div class="programSplitChild">

## Downloads

{{ downloadpanel(
    linux="https://ftp.code-scribe.com/speak-n-spell/Speak N Spell Linux.zip",
    macos="https://ftp.code-scribe.com/speak-n-spell/Speak N Spell macOS.zip",
    source="https://gitlab.com/caton101/speak-n-spell-emulator"
)}}

</div>
<div class="programSplitChild">

## How To Install

1. Download the zip file for your operating system.
2. Extract the zip file.
3. Run the install script.

</div>
</div>
