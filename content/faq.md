+++
title="Frequently Asked Questions"
+++

Here I will attempt to answer common questions in a question and answer format.
Please check if your question is on this page before contacting me.

---

<div class="faqDiv">
<div class="faqBlock">

## How old are you?

24

</div>
<div class="faqBlock">

## Where do you live?

North Carolina, United States of America

</div>
<div class="faqBlock">

## What is your favorite programming language?

Python for scripting. C++ for games.

</div>
<div class="faqBlock">

## What operating system do you use?

Linux

</div>
<div class="faqBlock">

## What Linux distribution do you use?

I use several Linux distributions. My development computers and cloud server run
Arch Linux. My file server runs Red Hat Enterprise Linux. My DNS server runs
Debian. My Raspberry Pis run Raspberry Pi OS. I also have several handheld
systems which run a variety of distros (although most run Debian).

</div>
<div class="faqBlock">

## Why do you use Linux instead of Windows?

Freedom. I like being able to customize my operating system and set up a custom
workflow that works for me. This goes further than just the programs I use. I
like to configure my desktop environment, my application launcher, the panels
on my screen, and then change the way my system looks. Everything is custom and
it maximizes my workflow.

</div>
<div class="faqBlock">

## Why do you some software for Linux and not Windows?

I use Linux. Linux makes software development a lot more enjoyable than on
Windows. Unfortunately, it is difficult for me to port Linux applications to
Windows unless the programming language supports cross-platform building. macOS
is supported only by being UNIX-like which means my Linux software will usually
build and run with no issues. This leaves Windows in the dust.

</div>
<div class="faqBlock">

## Will you port your games to Windows?

It is on my list of things to do. Unfortunately, life is very busy for me and
expanding my portfolio of software has a higher priority. Once I have a
decent portfolio of Linux software, I will port most of my library to  Windows.

</div>
<div class="faqBlock">

## Will you port your software to macOS?

Technically, most of my software already runs on macOS. Apple does a great job
of supporting most GNU toolchains and maintaining POSIX compliance. If it
doesn't outright support macOS, there is a very good chance it can be bult from
source locally and run with no errors. I always ensure software made with
cross-platform tools has a macOS version.

</div>
<div class="faqBlock">

## Will you port your games to console?

Depends on the console. Xbox would be the easiest because Microsoft provides a
lot of great tools to port Windows games to Xbox. I will consider Switch ports
because I am a huge fan of Nintendo hardware. PlayStation ports are not likely.
I don't own anything newer than a PlayStation 3 so development would be
difficult and expensive. The more money it costs to port, the less likely I am
to do it.

</div>
<div class="faqBlock">

## What programming languages do you know?

Assembly (6502, x86_64)

Bash (Shell Scripts)

Batch (DOS/Windows Scripts)

C

C++

C#

Commodore BASIC

Go

Java

JavaScript

Kotlin

Python

Rust

</div>
<div class="faqBlock">

## Why is your website so strange?

I am very familiar with HTML, CSS, JavaScript, NodeJS, JQuery, etc because of
college elective classes. Unfortunately, I have no passion for web development.
I can make a proper website if I absolutely have to, but I decided to put little
effort into my personal site due to a lack of passion. My main passion lies in
standalone software development such as terminal applications and games.

</div>
</div>
