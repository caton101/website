+++
title="My Blog"
+++

My blog is maintained on a separate website to keep my code easier to manage.
Content varies based on my interests and activities. Some topics may include:
circuits, life stories, operating systems, photography, programming, servers,
etc.

{{ urlbutton(
    link="https://blog.code-scribe.com/",
    message="Read my blog!"
) }}
