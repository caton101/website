+++
title="Welcome to Code Scribe."
+++

I have been fascinated by computers ever since I was a kid. I can clearly
remember playing PlayStation 2 games on my dad's giant TV on Friday and Saturday
nights. By 8 years old, I knew I wanted to study computers. I have been studying
various fields of computing ever since. My journey began with IT troubleshooting
and fixing people's computers. Video games caught my attention when I was 11 and
I decided to focus on software. I challenged myself to learn programming and
study all aspects needed to make a video game. This goal will never be completed
because of how fast things are changing. I dedicated this website to my journey
in computer programming and the challenges I face daily. Check out any of the
above links to learn more about me and my journey.

{{ specialimage(
    class="homepageImage",
    link="/assets/home.png",
    caption="Screenshot from The Cult Below"
) }}

