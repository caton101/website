+++
title="Demo Name"
draft=true
+++

{{ specialimage(
    class="programBanner",
    link="/assets/program-art/unknown/banner.jpg"
)}}

This is a description of what the the demo does.

<div class="programSplitView">
<div class="programSplitChild">

## Downloads

**Note**: To disable a download, just don't set that variable.

{{ downloadpanel(
    linux="https://ftp.code-scribe.com/program-name/Program Name Linux.zip",
    windows="https://ftp.code-scribe.com/program-name/Program Name Windows.zip",
    macos="https://ftp.code-scribe.com/program-name/Program Name macOS.zip",
    source="https://gitlab.com/caton101/repo"
)}}

</div>
<div class="programSplitChild">

## How To Run

1. Step 1.
2. Step 2.
3. Step 3.

</div>
</div>
