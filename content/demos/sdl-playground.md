+++
title="SDL Playground"
+++

{{ specialimage(
    class="programBanner",
    link="/assets/program-art/sdl-playground/banner.jpg"
)}}

I made a lot of demos when I started learning SDL2. This is a giant collection
of demos that show how to work with SDL2/C++.

<div class="programSplitView">
<div class="programSplitChild">

## Downloads

{{ downloadpanel(
    linux="https://ftp.code-scribe.com/sdl-playground/SDL Playground Linux.zip",
    source="https://gitlab.com/caton101/sdl-playground"
)}}

</div>
<div class="programSplitChild">

## How To Run

1. Install SDL2 libraries for your distro.
2. Install gcc for your distro.
3. Download and unzip the demo file.
4. Compile the demos.
5. Run the fresh binary file.

</div>
</div>
