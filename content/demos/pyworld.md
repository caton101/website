+++
title="PyWorld"
+++

{{ specialimage(
    class="programBanner",
    link="/assets/program-art/pyworld/banner.jpg"
)}}

PyWorld is a terrain visualizer using Perlin noise. Use W/A/S/D to move the
camera and Q/E to zoom.

<div class="programSplitView">
<div class="programSplitChild">

## Downloads

{{ downloadpanel(
    linux="https://ftp.code-scribe.com/pyworld/PyWorld Linux.zip",
    windows="https://ftp.code-scribe.com/pyworld/PyWorld Windows.zip",
    macos="https://ftp.code-scribe.com/pyworld/PyWorld macOS.zip",
    source="https://gitlab.com/caton101/PyWorld"
)}}

</div>
<div class="programSplitChild">

## How To Run

1. Install Python 3.
2. Install the Pygame module.
3. Install the noise module.
4. Download and unzip the program.
5. Run the launch script for your platform.

</div>
</div>
