+++
title="Color Tiles"
+++

{{ specialimage(
    class="programBanner",
    link="/assets/program-art/color-tiles/banner.jpg"
)}}

Color Tiles is a small graphics demo that I made using Python and Pygame. This
demo has several keybindings to control the rendering of tiles. Check the
included README file for a list of keybindings.

<div class="programSplitView">
<div class="programSplitChild">

## Downloads

{{ downloadpanel(
    linux="https://ftp.code-scribe.com/color-tiles/Color Tiles Linux.zip",
    windows="https://ftp.code-scribe.com/color-tiles/Color Tiles Windows.zip",
    macos="https://ftp.code-scribe.com/color-tiles/Color Tiles macOS.zip",
    source="https://gitlab.com/caton101/Color-Tiles"
)}}

</div>
<div class="programSplitChild">

## How To Run

1. Install Python 3.
2. Install the Pygame module.
3. Download and unzip the program.
4. Run the launch script for your platform.

</div>
</div>
