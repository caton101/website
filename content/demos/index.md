+++
title="Demos"
+++

These are small programs based on experimental concepts and are not considered
polished. There may be severe bugs or they may not work at all.

---

{{ programlistingr(
    name="Color Tiles",
    description="This is a small program that draws colorful tiles on the screen.",
    image="/assets/program-art/color-tiles/icon.png",
    link="/demos/color-tiles"
)}}

---

{{ programlisting(
    name="Fractal Engine",
    description="This is a very basic Mandelbrot set renderer.",
    image="/assets/program-art/fractal-engine/icon.png",
    link="/demos/fractal-engine"
)}}

---

{{ programlistingr(
    name="PyWorld",
    description="This is an experiment with terrain generation in Python.",
    image="/assets/program-art/pyworld/icon.png",
    link="/demos/pyworld"
)}}

---

{{ programlisting(
    name="SDL Playground",
    description="This is just a collection of SDL2 projects I wanted to make. There is no rhyme or reason to these programs.",
    image="/assets/program-art/sdl-playground/icon.png",
    link="/demos/sdl-playground"
)}}

