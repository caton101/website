+++
title="Fractal Engine"
+++

{{ specialimage(
    class="programBanner",
    link="/assets/program-art/fractal-engine/banner.jpg"
)}}

One day I decided to make a program that can render the Mandelbrot set. There
are no interactive features. The GUI does not zoom or pan.

<div class="programSplitView">
<div class="programSplitChild">

## Downloads

{{ downloadpanel(
    linux="https://ftp.code-scribe.com/fractal-engine/Fractal Engine Linux.zip",
    windows="https://ftp.code-scribe.com/fractal-engine/Fractal Engine Windows.zip",
    macos="https://ftp.code-scribe.com/fractal-engine/Fractal Engine macOS.zip",
    source="https://gitlab.com/caton101/Fractal-Engine"
)}}

</div>
<div class="programSplitChild">

## How To Run

1. Install Python 3.
2. Install the Pygame module.
3. Download and unzip the program.
4. Run the launch script for your platform.

</div>
</div>
