+++
title="Minesweeper"
+++

{{ specialimage(
    class="programBanner",
    link="/assets/program-art/minesweeper/banner.jpg"
)}}

 This started as a challenge on my Discord server. I tasked my friends with
 creating a terminal port of Minesweeper. It is required that no terminal
 libraries be used for input or graphics. Everything had to be implemented as if
 it were 1984 all over again.

<div class="programSplitView">
<div class="programSplitChild">

## Gameplay

Once started, the player must enter the width and height of the board and the
number of mines. WASD will move the cursor up, left, down and right
respectively. Typing "flag" will place a flag over a potential mine. Typing
"select" will check if the selected tile is a mine. Typing "exit" will exit the
game. Note: Press ENTER after any command or the game will not work.

</div>
<div class="programSplitChild">

## Downloads

{{ downloadpanel(
    linux="https://ftp.code-scribe.com/minesweeper/Minesweeper Linux.zip",
    windows="https://ftp.code-scribe.com/minesweeper/Minesweeper Windows.zip",
    source="https://gitlab.com/caton101/minesweeper"
)}}

</div>
</div>
