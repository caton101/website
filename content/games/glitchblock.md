+++
title="Glitchblock"
+++

{{ specialimage(
    class="programBanner",
    link="/assets/program-art/glitchblock/banner.jpg"
)}}

As with most of my projects, this started out of boredom. I had an idea to
recreate the visual effect of old home consoles connected to a CRT television.
After seeing potential to turn the demo into a game, Glitchblock was created.

<div class="programSplitView">
<div class="programSplitChild">

## Gameplay

This game first pauses for the player to press SPACE. Upon pressing the SPACE
button, the player appears and the game begins. The goal is to stay in the
screen by jumping from block to block. Pressing WASD will move the player up,
left, down, and right respectively. SPACE can be used to jump to higher
platforms. When the player moves off the screen (except for the top), the game
is over. Pressing SPACE on the death screen will close the game.

</div>
<div class="programSplitChild">

## Downloads

{{ downloadpanel(
    linux="https://ftp.code-scribe.com/glitchblock/Glitchblock Linux.zip",
    windows="https://ftp.code-scribe.com/glitchblock/Glitchblock Windows.zip",
    source="https://gitlab.com/caton101/glitchblock"
)}}

</div>
</div>
