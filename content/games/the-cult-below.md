+++
title="The Cult Below"
+++

{{ specialimage(
    class="programBanner",
    link="/assets/program-art/the-cult-below/banner.jpg"
)}}

 In Summer 2020, I decided to test my game development skills and enter a
 competition. The goal was simple: create a horror game in 10 days. I took an
 old tabletop RPG story that I wrote and turned it into a playable video game.
 This game was kept in its original state and has not been modified in any way.
 Due to time limitations, this game was never complete. The story is told
 through environmental storytelling only.

<div class="programSplitView">
<div class="programSplitChild">

## Gameplay

Explore the underground bunker and figure out how to escape. Good luck!

</div>
<div class="programSplitChild">

## Downloads

{{ downloadpanel(
    linux="https://codescribe.itch.io/the-cult-below",
    windows="https://codescribe.itch.io/the-cult-below",
    macos="https://codescribe.itch.io/the-cult-below"
)}}

</div>
</div>
