+++
title="Coin Game"
+++

{{ specialimage(
    class="programBanner",
    link="/assets/program-art/coin-game/banner.jpg"
)}}

This game was made after I finished an online Python programming course. I
decided a small text-based game was best since I did not know any graphics
libraries at the time of development. Armed with only text, I decided to model a
game after the Atari 2600. The game has seen many changes, but the core gameplay
has not changed. Some features have been added and removed but I try to maintain
the feeling of the original version. Due to technical reasons, this game is
exclusive to Linux.

<div class="programSplitView">
<div class="programSplitChild">

## Gameplay

This game runs from a Linux terminal. The graphics slowly generate on the screen
and a startup sound is played. The player can use WASD to move up, left, down,
and right respectively. The goal is to reach the maximum score before the
computer does. Score points are earned by walking into coins that are placed
randomly on the screen. More points are given if there are fewer coins on the
screen. Pressing ESCAPE will exit the game and P will skip the player's turn.

</div>
<div class="programSplitChild">

## Downloads

{{ downloadpanel(
    linux="https://ftp.code-scribe.com/coin-game/Coin Game Linux.zip",
    source="https://gitlab.com/caton101/Coin-Game"
)}}

</div>
</div>
