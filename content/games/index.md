+++
title="Games"
+++

These are fully working games that I consider to be complete. Check them out and
let me know what you think of the experience.

---

{{ programlistingr(
    name="Arcade Boy",
    description="A small arcade-style platformer. It was made with a group of friends for a game competition. It was never finished, but it is playable.",
    image="/assets/program-art/arcade-boy/icon.png",
    link="/games/arcade-boy"
)}}

---

{{ programlisting(
    name="Coin Game",
    description="This is my very first video game. Inspired by the Atari 2600, this game features text-based graphics and retro sounds.",
    image="/assets/program-art/coin-game/icon.png",
    link="/games/coin-game"
)}}

---

{{ programlistingr(
    name="Cube Catcher",
    description="In 2017, I tasked myself with a challenge. The goal was to make a game in 90 minutes. It was influenced by simplistic mobile games.",
    image="/assets/program-art/cube-catcher/icon.png",
    link="/games/cube-catcher"
)}}

---

{{ programlisting(
    name="Glitchblock",
    description="Born from my love of retro games, Glitchblock features old color graphics and endurance-based gameplay.",
    image="/assets/program-art/glitchblock/icon.png",
    link="/games/glitchblock"
)}}

---

{{ programlistingr(
    name="Minesweeper",
    description="A classic puzzle game for the Linux terminal. Flag all the mines and do not blow up.",
    image="/assets/program-art/minesweeper/icon.png",
    link="/games/minesweeper"
)}}

---

{{ programlisting(
    name="The Cult Below",
    description="A custom tabletop RPG story, but in video game form.",
    image="/assets/program-art/the-cult-below/icon.png",
    link="/games/the-cult-below"
)}}

