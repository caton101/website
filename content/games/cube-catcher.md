+++
title="Cube Catcher"
+++

{{ specialimage(
    class="programBanner",
    link="/assets/program-art/cube-catcher/banner.jpg"
)}}

This game is the result of a small challenge designed to cure my boredom on an
unusually slow day. The challenge was simple - create a video game in 90
minutes. I knew there wasn't enough time for excellent graphics or elaborate
gameplay mechanics. Settling for a mobile time-waster, I began programming. At
the end of 90 minutes, the game was complete except for making the menus work. I
continued programming for five extra minutes in order to fix any overlooked
issues.

<div class="programSplitView">
<div class="programSplitChild">

## Gameplay

The game starts with a small title screen asking the player to press space. Upon
pressing the correct key, the game will begin. Pressing A and D will move the
paddle left and right respectively. Move under the falling white cubes in order
to score points. If you miss a cube, the game ends. You can press escape at
anytime to return to the title screen. If you lose (this will happen at some
point), press space to return to the main menu.

</div>
<div class="programSplitChild">

## Downloads

{{ downloadpanel(
    linux="https://ftp.code-scribe.com/cube-catcher/Cube Catcher Linux.zip",
    windows="https://ftp.code-scribe.com/cube-catcher/Cube Catcher Windows.zip",
    macos="https://ftp.code-scribe.com/cube-catcher/Cube Catcher macOS.zip"
)}}

</div>
</div>
