+++
title="Arcade Boy"
+++

{{ specialimage(
    class="programBanner",
    link="/assets/program-art/arcade-boy/banner.jpg"
)}}

This game was produced with a group of friends for LMOB Beginner's Game Jam #28.
I performed some of the level design, made most of the art, configured the Unity
engine, and write some of the gameplay scripts. The game was never finished, but
it ended in a playable state with a single level.

<div class="programSplitView">
<div class="programSplitChild">

## Gameplay

Upon launch, the player uses an interactive menu to start the game. Moving right
will start the game. Moving left will exit the game. When started, the player
can use the space key to jump and left-click to shoot. There are buttons which
need to be triggered in order to solve the level.

</div>
<div class="programSplitChild">

## Downloads

{{ downloadpanel(
    linux="https://codescribe.itch.io/arcade-boy",
    windows="https://codescribe.itch.io/arcade-boy",
    macos="https://codescribe.itch.io/arcade-boy"
)}}

</div>
</div>
