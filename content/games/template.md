+++
title="Game Name"
draft=true
+++

{{ specialimage(
    class="programBanner",
    link="/assets/program-art/unknown/banner.jpg"
)}}

This is for explaining the development process.

<div class="programSplitView">
<div class="programSplitChild">

## Gameplay

This describes what the player can expect while playing the game.

</div>
<div class="programSplitChild">

## Downloads

**Note**: To disable a download, just don't set that variable.

{{ downloadpanel(
    linux="https://ftp.code-scribe.com/program-name/Program Name Linux.zip",
    windows="https://ftp.code-scribe.com/program-name/Program Name Windows.zip",
    macos="https://ftp.code-scribe.com/program-name/Program Name macOS.zip",
    source="https://gitlab.com/caton101/repo"
)}}

</div>
</div>
