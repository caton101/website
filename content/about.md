+++
title="About Me"
+++

{{ specialimage(
    class="me",
    link="/assets/me/me.jpg",
    caption="A headshot of me"
) }}

I grew up on a small farm in the middle of North Carolina. My childhood years
were full of discovery and creating new things. I got in trouble for some very
strange things like removing a thermostat from the wall and taking apart my
toys. I wanted to know how everything worked. Nothing could stop my curiosity.
I spent most weekends helping my grandpa on the farm. I helped repair farm
equipment and helped him build whatever the farm needed.

It was no surprise that I would be interested in computers. I was able to repair
my desktop by eight years old and I wrote my first computer program at eleven. I
was ten when I could walk into an electronics shop and speak on the same level
as the on-site repair technicians. I continue studying math and science to this
day. Nothing can stop me from learning new skills.

School was a terrible place for me. I was both the quiet kid and the fat kid so
I was subject to the most bullying in school. To make matters worse, I went to a
small private school so I was stuck with the same classmates every single year.
I transferred to a public school after ninth grade but the damage was done. I
continued my tradition of being the quiet kid. I only talked to people if they
started a conversation. I never went out of my way to socialize. I went to
school to learn and everything else was extra. I missed out on many common
childhood memories, but I am satisfied in my decision to only use school for
learning.

No social life during my childhood gave me significant amounts of time to learn
things I was interested in. School was very easy for me and I never studied for
tests. During class, I would normally write computer programs on paper. I was
always excited to go home and type my code into my computer and see if it
worked. In high school, I was given a laptop for instructional purposes. Yeah,
like that was going to end well. I started using an online IDE to write and test
programs during class. I even had challenges to keep myself entertained. I
started and finished a Python programming course in two weeks during the same
class period. I felt legendary when I managed to complete that challenge.
After competing the Python course, I kept writing code in an online IDE. I
started making games that I could play during class. The online IDE only had a
terminal over SSH so all my games had to be terminal based. I may not have grown
up in the 80's, but I felt like an 80's kid learning to code on their Commodore
64. "Coin Game" was made entirely during school. There were also many
programming demos that were made because of boredom. Some of these are public
and others are not.

In May 2022, I graduated from Appalachian State University with a bachelor's
degree in computer science. During my undergrad, I made friends that had the same
interests as me. We all worked on our own projects and shared progress with one
another. This drove all of us to push the boundaries of our programming skills.
When I wasn't busy with homework or personal projects, I was leading two clubs
on campus, Mountain Linux Club and ASCII. I also planned AppHack, a hackathon
hosted by student leaders. College changed my perspective on what I thought was
possible. I finally had professional social skills and a lot of computer
knowledge about things I had no idea existed.

Today, I mostly work on personal projects. Most software I write are either
Linux system daemons or command line utilities. Shaped by my college degree, I
see computers as tools that can achieve anything I want. A sci-fi world is only
some lines of code away from reality.

