+++
title="Contact Information"
+++

Thank you! You found your way to my website and now you are trying to contact me. That is something 90% of my family can't be bothered to do. Since you are so kind, here is a list of many different places you can find me.

---

<div class="contactDiv">
<div class="contactBlock">

## itch.io

If you came here expecting games, this is the link you want. My itch.io page is a work in progress but I expect it to grow significantly.

{{ urlbutton(
    link="https://codescribe.itch.io/",
    message="Play my games!"
) }}

</div>
<div class="contactBlock">

## Email

Email is the best way to contact me. I constantly monitor my email address and should be able to reply within a day. Please be sure to state who you are and provide a meaningful subject line. If I see an email with "My email" as the subject, there is no way I can take it seriously.

{{ urlbutton(
    link="mailto:codescribebiz@gmail.com",
    message="Send me an email!"
) }}

</div>
<div class="contactBlock">

## Mastodon

Mastodon is essentially an open source Twitter. Powered by the Fediverse, it brings multiple server instances together. I am most active over there.

{{ urlbutton(
    link="https://tech.lgbt/@biti",
    message="Follow me on the Fediverse!"
) }}

</div>
<div class="contactBlock">

## GitLab

GitLab is home to all of my code projects. Every program and game mentioned on this website has its source code published there. If you found a bug with any of my projects, please fill out an issue report. If you do not use GitLab, send an email and I will make a note to fix it.

{{ urlbutton(
    link="https://gitlab.com/BitiTiger",
    message="View my code!"
) }}

</div>
<div class="contactBlock">

## GitHub

GitHub was home to many of my earliest projects. When Microsoft bought GitHub, I was one of many people in the free software community to make the journey to GitLab. I have a repository called "Farewell" with a note explaining my decision to move my code to another platform. Occasionally, I will star an interesting project or contribute to a project. My GitHub is very boring but you can check it out if that interests you.

{{ urlbutton(
    link="https://github.com/BitiTiger",
    message="View my code!"
) }}

</div>
</div>
