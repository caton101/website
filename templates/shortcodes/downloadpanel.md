{% if linux %}
<a class="programDownloadButton" href="{{linux}}">
    Linux
</a>
{% else %}
<div class="programDownloadButton" disabled>
    Linux
</div>
{% endif %}

{% if windows %}
<a class="programDownloadButton" href="{{windows}}">
    Windows
</a>
{% else %}
<div class="programDownloadButton" disabled>
    Windows
</div>
{% endif %}

{% if macos %}
<a class="programDownloadButton" href="{{macos}}">
    macOS
</a>
{% else %}
<div class="programDownloadButton" disabled>
    macOS
</div>
{% endif %}

{% if source %}
<a class="programDownloadButton" href="{{source}}">
    Source Code
</a>
{% else %}
<div class="programDownloadButton" disabled>
    Source Code
</div>
{% endif %}
